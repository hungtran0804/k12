const UserModel = require("./../../core/models/user")
const jwt = require("./../../core/jwt/index")
const status_code = require("./../../core/constants/status_code");
const { throwError } = require("../../../helpers");
const { createToken } = require("./../../core/jwt/index");
module.exports = {

  hashPassword(password) {
    return bcrypt.hashSync(password, 10);
  },

  comparePassword(password, hash) {
    return bcrypt.compareSync(password, hash);
  },

  async findExitstedUser(email = null){
    if (!email){
      return null
    }
    let user = await UserModel.find({
      email: email
    })
    return user
  },

  async login(email = null, password = null){
    let user = await self.findExitstedUser(email)
    if (!user){
      throwError(500, 'incorrect email or password!', null, status_code.INCORRECT_LOGIN_DATA)
    }
    let is_password_correct = self.comparePassword(password, user.password);
    if(!is_password_correct) {
        throwError(400, 'incorrect user or password!', null, statusCodes.INCORRECT_LOGIN_DATA);
    }

    return user;
  },

  async createToken(user){
    let payload = {
      _id: user._id,
      email: user.email,
      phone: user.phone,
      level: user.level,
      language: user.language,
    }
    let token = await jwt.createToken(payload);
    return token;
  },
  
  async register({email, phone, password, language}) {
    let user = await self.findExisted(email, phone);
    if (user) {
        let message = (user.email === email) ? 'existed email!' : 'existed phone!';
        let status_code = (user.email === email) ? statusCodes.EXISTED_EMAIL : statusCodes.EXISTED_PHONE;
        throwError(400, message, null, status_code);
    }
    password = self.hashPassword(password);
    let new_user = await model.create({ email, phone, password, language });
    return new_user;
  },
}