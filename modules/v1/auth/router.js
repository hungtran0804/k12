const controller = require('./controller');

module.exports = [
  {
    type: 'group',
    prefix: '/v1/auth',
    preHandler: [],
    children: [
      { method: 'POST', url: '/login', handler: controller.logins }
      // { method: 'POST', url: '/register', handler: controller.register },
    ]
  },
]