const { throwError, validateInput, successResponse } = require('./../../../helpers');

module.exports = {
  async login(req,res){
    let { user_name, password } = input_data = validateInput(
      req.body,
      {
          user_name: { required: true },
          password: { required: true },
      }
    );
    let user = await service.login(user_name, password);
    let token = await service.createToken(user);

    return successResponse({
        userData: user,
        token: token,
    });
  }
}