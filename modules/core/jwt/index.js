const config = require('./config');
const jwt = require('jsonwebtoken');

const self = module.exports = {

  async createToken(payload) {
      let token = await jwt.sign(payload, config.private_key, config.options);
      return token;
  },

  async verifyToken(token) {
      let data = jwt.verify(token, config.private_key);
      return data;
  }
  
}