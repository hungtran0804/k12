const mongoose = require("mongoose")

const schema = mongoose.Schema({
  email: {type: String, required: true, unique: true, trim: true},
  password: {type: String, required: true, trim: true},
  first_name: {type: String, trim: true, default: null},
  last_name: {type: String, trim: true, default: null},
  full_name: {type: String, trim: true, default: null},
  address: {type: String, trim: true, default: null},
  background: {type: String, default: null},
  avatar: {type: String, default:"https://dev-api-gateway.x3english.club/storage/avatar/1277881.jpg?t=1593675265"},
  birth_date: {type: Date, default: null},
  level: {type: Number, default: 1},
  language: {type: String, required: true},
  phone: {type: String, trim: true, default: null},
  currency: {type: String, default: "usd"},
  gender: {type: Number, default: 0},
  job: {type: String, default: null},
  updated_at: {type: Date, default: null},
  group: [],
  status: {type: Number, default: 1}
})

const model = mongoose.model("user", schema);
module.exports = model