const { throwError, validateInput, checkUpdateResult, checkDeleteResult } = require('../../../helpers');

module.exports = function(model) {
    return {
        async getAll(query = {}, project = null, sort = null) {
            let list = await model.find(query, project)
                .sort(sort)
                .catch(error => {
                    throwError(512, 'fail to get documents!', error.message);
                });

            return list;
        },

        async getList(query = {}, options = {}) {
            let { project, sort, page, limit } = validateInput(
                options,
                {
                    project: { default: null },
                    sort: {},
                    page: { type: Number, default: 1 },
                    limit: { type: Number, default: 180 },

                }
            );
            
            if(page < 1) { page = 1 }
            skip = (page - 1)*limit;

            let list = await model.find(query, project)
                .sort(sort)
                .skip(skip)
                .limit(limit)
                .catch(error => {
                    throwError(512, 'fail to get documents!', error.message);
                });

            return list;
        },

        async getOne(query = {}, project = null) {
            let documnent = await model.findOne(query, project).catch(error => {
                throwError(512, 'fail to get document!', error.message);
            });

            return documnent;
        },

        async getById(id, project = null) {
            let documnent = await model.findOne({
                _id: id,
            }, project).catch(error => {
                throwError(512, 'fail to get document!', error.message);
            });

            return documnent;
        },

        async create(input_data = {}) {
            let new_document = await model.create(input_data).catch(error => {
                throwError(512, 'fail to create document!', error.message);
            });
            return new_document;
        },

        async update(id, input_data = {}, options = {}) {
            let update_result = await model.updateOne({
                _id: id,
            }, input_data, options).catch(error => {
                throwError(512, 'fail to update document!', error.message);
            })
            return checkUpdateResult(update_result);
        },

        async updateOne(query = {}, input_data = {}, options = {}) {
            let update_result = await model.updateMany(query, input_data, options).catch(error => {
                throwError(512, 'fail to update document!', error.message);
            })
            return checkUpdateResult(update_result);
        },

        async updateMany(query = {}, input_data = {}, option = {}) {
            let update_result = await model.updateMany(query, { $set: input_data }, option).catch(error => {
                throwError(512, 'fail to update documents!', error.message);
            })
            return checkUpdateResult(update_result);
        },

        async delete(id) {
            let result = await model.deleteOne({
                _id: id
            }).catch(error => {
                throwError(512, 'fail to delete document!', error.message);
            });
            return checkDeleteResult(result);
        },

        async count(query) {
            let result = await model.countDocuments(query).catch(error => {
                throwError(512, 'fail to count document!', error.message);
            })
            return result;
        },
    }
}