// Connect to Mongoose and set connection variable
let mongoose = require("mongoose");
mongoose.connect(process.env.DB_CONNECTION, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
});
var db = mongoose.connection;
if (!db) console.log("Error connecting db");
else console.log("Db connected successfully");