const self = module.exports = {
  trueTypeOf(obj) {
      return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
  },
  isObject(data) {
      return self.trueTypeOf(data) === 'object';
  },
}