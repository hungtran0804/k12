module.exports = async function(callback, name = 'fn') {
  const start_time = Date.now();
  console.log(`start "${name}"`);
  await callback();
  //-----------------------------
  console.log(`done "${name}" after: ${Date.now() - start_time} ms`);
}