function getTypeOf(obj) {
  return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase()
}

function isObject(obj) {
  return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase() === 'object'
}

function validateSchemaField(schemaField) {
  if (schemaField.required !== undefined && schemaField.default !== undefined) {
    throw new Error("invalid schema object: 'required' and 'default' cannot co-existed!")
  }
}

function checkRequiredRule(schemaField, objField) {
  if (schemaField.required === undefined || schemaField.required === false) {
    return true
  }

  if (objField === undefined) { return false }
  return true
}

function castToNumber(value, path) {
  let number = Number(value)
  if (number.isNaN) {
    throw new Error({
      msg: 'cast to number failed',
      path,
    })
  }
  return number
}

function validateArray(array, schema, option = { rootPath: null, strict: false }) {
  if (!Array.isArray(array)) {
    throw new Error('input_data must be array!')
  }
  let errors = []
  const schemaType = getTypeOf(schema)
  let result_array = array.map((item, index) => {
    const path = `${option.rootPath}[${index}]`
    let itemType = getTypeOf(item)
    if (schemaType === 'function') {
      if (!['string', 'number', 'boolean'].includes(itemType)) {
        errors.push({
          msg: `expect primitive value but got ${itemType} `,
          path,
        })
        return NaN
      }
      if (schema === Number) {
        return castToNumber(item)
      }
      return schema(item)
    }

    if (schemaType === 'object') {
      if (itemType !== 'object') {
        errors.push({
          msg: `expect object but got ${itemType} `,
          path,
        })
        return NaN
      }

      try {
        // eslint-disable-next-line no-use-before-define
        return validateObject(item, schema, { rootPath: path, strict: option.strict })
      } catch (error) {
        if (getTypeOf(error.data) === 'array' && error.data.length) {
          errors = errors.concat(error.data)
        } else {
          throw error
        }
      }
    }

    return item
  })

  if (errors.length) {
    let error = new Error('invalid input')
    error.data = errors
    throw error
  }
  return result_array
}

function isParentObject(obj) {
  if (getTypeOf(obj) !== 'object') {
    throw new Error('obj must be an object!')
  }

  let properties = Object.keys(obj)
  for (let i = 0, length = properties.length; i < length; i += 1) {
    let prop = properties[i]
    if (getTypeOf(obj[prop]) === 'object') {
      return true
    }
  }
  return false
}

function validateObject(obj, schema, option = { strict: false, rootPath: null }) {
  if (!isObject(obj) && obj !== undefined) {
    throw new Error('input_data must be an object!')
  }

  if (obj === undefined) {
    // eslint-disable-next-line no-param-reassign
    obj = {}
  }

  let result_object = {}
  let errors = []

  let properties = Object.keys(schema)
  for (let i = 0, length = properties.length; i < length; i += 1) {
    let prop = properties[i]
    const path = `${option.rootPath ? `${option.rootPath}.` : ''}${prop}`
    const objFieldType = getTypeOf(obj[prop])

    if (getTypeOf(schema[prop]) === 'array') {
      if (obj[prop] === undefined) {
        result_object[prop] = []
        continue
      }

      if (objFieldType !== 'array') {
        errors.push({
          msg: 'field must be an array but got another type!',
          field: path,
        })
        continue
      }

      try {
        result_object[prop] = validateArray(obj[prop], schema[prop][0], { rootPath: path, strict: option.strict })
      } catch (error) {
        if (getTypeOf(error.data) === 'array' && error.data.length) {
          errors = errors.concat(error.data)
        } else {
          throw error
        }
      }
      continue
    }
    if (isParentObject(schema[prop])) {
      try {
        result_object[prop] = validateObject(obj[prop], schema[prop], { rootPath: path, strict: option.strict })
      } catch (error) {
        if (getTypeOf(error.data) === 'array' && error.data.length) {
          errors = errors.concat(error.data)
        } else {
          throw error
        }
      }
      continue
    }
    validateSchemaField(schema[prop])
    if (obj[prop] === undefined) {
      if (!checkRequiredRule(schema[prop], undefined)) {
        errors.push({
          msg: 'field is required but got undefined!',
          field: path,
        })
      }
      if (schema[prop].default !== undefined) {
        result_object[prop] = schema[prop].default
      }
      continue
    }

    // check type rule
    const schemaTypeValue = schema[prop].type
    if (schemaTypeValue === undefined) { continue }

    if (schemaTypeValue === String) {
      result_object[prop] = objFieldType === 'string' ? obj[prop] : String(obj[prop])
      continue
    }

    if (schemaTypeValue === Number) {
      try {
        result_object[prop] = objFieldType === 'number' ? obj[prop] : castToNumber(obj[prop], path)
      } catch (error) {
        errors.push(error)
      }
      continue
    }

    if (schemaTypeValue === Boolean) {
      result_object[prop] = objFieldType === 'boolean' ? obj[prop] : Boolean(obj[prop])
      continue
    }
  }

  if (errors.length) {
    let error = new Error(`invalid input ${JSON.stringify(errors)}`)
    error.data = errors
    error.status = 400
    throw error
  }

  if (option.strict) {
    return result_object
  }
  return { ...obj, ...result_object }
}

module.exports = validateObject
