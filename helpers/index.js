const validateInput = require('./validate_input');
const { trueTypeOf, isObject } = require('./true_type_of');
const { filterByWhitelist, filterByBlacklist } = require('./object_filter');

const self = module.exports = {
    validateInput,
    trueTypeOf,
    isObject,
    filterByWhitelist,
    filterByBlacklist,
    throwError(status = 500, message = 'server error', data = null, statusCode = null) {
        let error = new Error(message);
        error.status = status;
        error.data = data;
        error.statusCode = statusCode || status;
        throw error;
    },
    checkUpdateResult(result) {
        if (result && result.n == 1 && result.nModified == 1) {
            return true;
        } else {
            return false;
        }
    },
    checkDeleteResult(result) {
        if (result && result.n == 1 && result.deletedCount == 1) {
            return true;
        } else {
            return false;
        }
    },
    successResponse(data, message = 'success!', status_code = 200) {
        return {
            success: true,
            statusCode: status_code,
            data: data,
            message: message,
        }
    },
}