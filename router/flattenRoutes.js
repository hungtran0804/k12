function pushGroupInfoToRoute(route, group) {
  if (!group) {
    return route;
  }
  if (group.prefix) {
    route.url = `${group.prefix}${route.url}`;
  }
  if (group.preHandler && group.preHandler.length) {
		if (route.preHandler) {
				route.preHandler.unshift(...group.preHandler)
		} else {
				route.preHandler = group.preHandler;
		}
  }
  return route;
}

function pushGroupInfoToRoutes(routes, group) {
  routes = routes.map(route => {
    return pushGroupInfoToRoute(route, group);
  })
  return routes;
}

function buildRoutes(routesArray, group = null) {
  let routes = [];
  routesArray.forEach(route => {
		if (!route.type) {
			if (group) {
				route = pushGroupInfoToRoute(route, group);
			}
			routes.push(route);
			return;
		} else if (route.type !== 'group') {
			console.log('invalid route definition:', route);
			return;
		}
		let children = buildRoutes(route.children, {
			prefix: route.prefix,
			preHandler: route.preHandler
		});
		if (!children.length) { return }
		if (group) {
			children = pushGroupInfoToRoutes(children, group);
		}
		routes.push(...children);
  })
  return routes;
}

module.exports = buildRoutes;