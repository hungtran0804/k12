const flattenRoutes = require('./flattenRoutes');

const routes = [
  ...require('../modules/v1/auth/router')
]

module.exports = async function (fastify, options) {
  flattenRoutes(routes).forEach(route => {
    fastify.route(route);
    console.log(route)
  });
}