require("./_bootstrap")
const fastify = require("fastify")({
    logger: {
        prettyPrint: true,
        serializers: {
            req(request) {
                return {
                    info: `${request.method} ${request.url}`,
                    query: request.query,
                    body: request.body,
                }
            },
            reqId(request) {},
            res(res) {

            }

        }
    },
    ignoreTrailingSlash: true,
})
fastify.register(require('fastify-formbody'));
fastify.register(require('fastify-multipart'));
fastify.register(require('fastify-cors'), {
  // put your options here
})

fastify.addContentTypeParser('text/plain', { parseAs: 'string' }, function (req, body, done) {
	try {
		var json = JSON.parse(body)
		done(null, json)
	} catch (err) {
		err.statusCode = 400
		done(err, undefined)
	}
})
//routing
fastify.get('/', async (request, reply) => {
  return { hello: 'world' }
})
fastify.register(require('./router/api'), { prefix: '/api' });
  fastify.setErrorHandler(function (error, req, res) {
    this.log.error(error);
    console.log(error.data);
    if (!error.status) {
      error.status = 500;
    }
	res.status(error.status).send({
		statusCode: error.statusCode,
		error: error.data || error.code || null,
		message: error.message,
	})
})

module.exports = fastify;